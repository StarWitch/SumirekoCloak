// for all the pretty patterns

void simple_test() {
  for(uint16_t x=0; x<CAPE_HEIGHT; x++) { // For each row...
    for(uint16_t y=0; y<CAPE_WIDTH; y++) { // For each pixel of row...
      leds.setPixelColor(cape_matrix[x][y], (255, 255, 255));
      if (x+1 < CAPE_WIDTH) {
        leds.setPixelColor(cape_matrix[x+1][y], (255, 255, 255));
      }
      if (x+2 < CAPE_WIDTH) {
        leds.setPixelColor(cape_matrix[x+2][y], (255, 255, 255));
      }
      if (x+3 < CAPE_WIDTH) {
        leds.setPixelColor(cape_matrix[x+2][y], (255, 255, 255));
      }
    }
  }
    
  leds.show(); // display LEDs
  delay(delay_func());
  fade_entire_matrix_off(16);
    
  for(uint16_t y=0; y<COLLAR_WIDTH; y++) { // For each row...
  for(uint16_t x=0; x<COLLAR_HEIGHT; x++) { // For each pixel of row...
    leds.setPixelColor(collar_matrix[x][y], (255, 255, 255));
      if (y+1 < COLLAR_WIDTH) { // just one over since it's large
        leds.setPixelColor(collar_matrix[x][y+1], (255, 255, 255));
      }
    }
  }
  leds.show(); // display LEDs
  delay(delay_func());
  fade_entire_matrix_off(16);
};

// keep this one

void fade_wipe() {
  for (int x = 0; x < 5; x++) {
      wipe_left(255, 8, 0);
      wipe_left(255, 8, 1);
      wipe_right(255, 8, 0);
      wipe_right(255, 8, 1);
      wipe_up(255, 8, 0);
      wipe_up(255, 16, 1);
      wipe_down(255, 8, 0);
      wipe_down(255, 16, 1);
  }
}

void sparkle() {
  for (int x = 0; x < 1000; x++) {
    uint16_t cape_x = random(0, CAPE_HEIGHT-1); // these need to be zero indexed. random(32) or (14) will overflow
    uint16_t cape_y = random(0, CAPE_WIDTH-1);
    leds.setPixelColor(cape_matrix[cape_x][cape_y], (255, 255, 255)); // set the anchor point
    // and then create a "starburst" sort of cross pattern
    if (cape_y < CAPE_WIDTH - 2) { // -2 so that it can still go one pixel over
       leds.setPixelColor(cape_matrix[cape_x][cape_y+1], (192, 192, 192));
    }
    if (cape_x < CAPE_HEIGHT - 2) {
       leds.setPixelColor(cape_matrix[cape_x+1][cape_y], (192, 192, 192));
    }
      if (cape_y > 1) {
       leds.setPixelColor(cape_matrix[cape_x][cape_y-1], (192, 192, 192));
    }
    if (cape_x > 1) {
       leds.setPixelColor(cape_matrix[cape_x-1][cape_y], (192, 192, 192));
    }
    if (cape_y < CAPE_WIDTH - 3) { // -2 so that it can still go one pixel over
       leds.setPixelColor(cape_matrix[cape_x][cape_y+2], (128, 128, 128));
    }
    if (cape_x < CAPE_HEIGHT - 3) {
       leds.setPixelColor(cape_matrix[cape_x+2][cape_y], (128, 128, 128));
    }
      if (cape_y > 2) {
       leds.setPixelColor(cape_matrix[cape_x][cape_y-2], (128, 128, 128));
    }
    if (cape_x > 2) {
       leds.setPixelColor(cape_matrix[cape_x-2][cape_y], (128, 128, 128));
    }
    
  
    uint16_t collar_y = random(0, COLLAR_HEIGHT-1);
    uint16_t collar_x = random(0, COLLAR_WIDTH-1);
    leds.setPixelColor(collar_matrix[collar_x][collar_y], (255, 255, 255));
    if (collar_y < CAPE_WIDTH-2) {
       leds.setPixelColor(collar_matrix[collar_x][collar_y+1], (192, 192, 192));
    }
    if (collar_x < CAPE_HEIGHT-2) {
       leds.setPixelColor(collar_matrix[collar_x+1][collar_y], (192, 192, 192));
    }
      if (collar_y > 1) {
       leds.setPixelColor(collar_matrix[collar_x][collar_y-1], (192, 192, 192));
    }
    if (collar_x > 1) {
       leds.setPixelColor(collar_matrix[collar_x-1][collar_y], (192, 192, 192));
    }
    
    leds.show();
    delay(delay_func());
    for (uint16_t i=0; i < STRIPS_TOTAL; i++) {
      progressive_fade_off(i, 4);
    }
  }

}

void new_plasma(){
  int default_cycles = 500;
  for (uint16_t x = 0; x < default_cycles; x++) {
    frameCount++; 
    uint32_t t = fastCosineCalc((42 * frameCount)/100);  //time displacement - fiddle with these til it looks good...
    uint32_t t2 = fastCosineCalc((35 * frameCount)/100); 
    uint32_t t3 = fastCosineCalc((38 * frameCount)/100);

    for (uint8_t y = 0; y < CAPE_WIDTH; y++) {
      int left2Right, pixelIndex;
      if (((y % (CAPE_WIDTH/8)) & 1) == 0) {
        left2Right = 1;
        pixelIndex = y * CAPE_HEIGHT;
      } else {
        left2Right = -1;
        pixelIndex = (y + 1) * CAPE_HEIGHT - 1;
      }
      for (uint8_t x = 0; x < CAPE_HEIGHT ; x++) {
        uint8_t uv_cape = fastCosineCalc(((x << 3) + (t >> 1) + fastCosineCalc((t2 + (y << 3)))));
        leds.setPixelColor(pixelIndex, ((uv_cape << 16) | (uv_cape << 8) | uv_cape));
        pixelIndex += left2Right;
      }
    }
      for (uint8_t y = 0; y < COLLAR_WIDTH; y++) {
      int left2Right_collar, pixelIndex_collar;
      if (((y % (COLLAR_WIDTH/8)) & 1) == 0) {
        left2Right_collar = 1;
        pixelIndex_collar = (y * COLLAR_HEIGHT) + 447;
      } else {
        left2Right_collar = -1;
        pixelIndex_collar = ((y + 1) * COLLAR_HEIGHT - 1) + 447;
      }
      for (uint8_t x = 0; x < COLLAR_HEIGHT ; x++) {
        uint8_t uv_collar = fastCosineCalc(((x << 3) + (t >> 1) + fastCosineCalc((t2 + (y << 3)))));
        leds.setPixelColor(pixelIndex_collar, ((uv_collar << 16) | (uv_collar << 8) | uv_collar));
        pixelIndex_collar += left2Right_collar;
      }
    }
    leds.show();
    delay(delay_func());  
  }
}

void matrix_shimmer() {
    for (int x = 0; x < 3; x++) {
      int uv_rand = random(0, 255);
      progressive_fade_up_or_down(uv_rand);
      progressive_fade_up_or_down(0);
    }
}
