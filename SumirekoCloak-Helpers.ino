long unsigned int delay_func() {
  return(analogRead(A5) / 8);
};

inline uint8_t fastCosineCalc( uint16_t preWrapVal) {
  uint8_t wrapVal = (preWrapVal % 255);
  if (wrapVal<0) wrapVal=255+wrapVal;
  return (pgm_read_byte_near(cos_wave+wrapVal)); 
}

void wipe_left(int intensity, int fade, boolean collar) {
  for (int16_t i=0; i < STRIPS_TOTAL; i++) {
      progressive_fade_off(i, fade);
  }
  if (!collar) {
  for (int y = CAPE_WIDTH-1; y > 0; y--) {
    for (int x = 0; x < CAPE_HEIGHT; x++) {
      leds.setPixelColor(cape_matrix[x][y], (intensity,intensity,intensity));
    }

    for (int16_t i=0; i < STRIPS_TOTAL; i++) {
      progressive_fade_off(i, fade);
    }
        leds.show();
    delay(delay_func());
  }
  } else {
  for (int y = COLLAR_WIDTH-1; y > 0; y--) {
    for (int x = 0; x < COLLAR_HEIGHT; x++) {
      leds.setPixelColor(collar_matrix[y][x], (intensity,intensity,intensity));
    }

    for (int16_t i=0; i < STRIPS_TOTAL; i++) {
      progressive_fade_off(i, fade);
    }
        leds.show();
    delay(delay_func() * 2);
  }
  }
}

void wipe_right(int intensity, int fade, boolean collar) {
  for (int16_t i=0; i < STRIPS_TOTAL; i++) {
      progressive_fade_off(i, fade);
  }
  if (!collar) {
    for (int y = 0; y < CAPE_WIDTH; y++) {
      for (int x = 0; x < CAPE_HEIGHT; x++) {
        leds.setPixelColor(cape_matrix[x][y], (intensity,intensity,intensity));
      }
  
      for (int16_t i=0; i < STRIPS_TOTAL; i++) {
        progressive_fade_off(i, fade);
      }
      leds.show();
      delay(delay_func() * 2);
    }
  } else {
    for (int y = 0; y < COLLAR_WIDTH; y++) {
      for (int x = 0; x < COLLAR_HEIGHT; x++) {
        leds.setPixelColor(collar_matrix[y][x], (intensity,intensity,intensity));
      }
  
      for (int16_t i=0; i < STRIPS_TOTAL; i++) {
        progressive_fade_off(i, fade);
      }
      leds.show();
      delay(delay_func() * 2);
    }
  }
}

void wipe_up(int intensity, int fade, boolean collar) {
  for (int16_t i=0; i < STRIPS_TOTAL; i++) {
      progressive_fade_off(i, fade);
  }
  if (!collar) {
    for (int x = CAPE_HEIGHT-1; x > 0; x--) {
      for (int y = 0; y < CAPE_WIDTH; y++) {
    
          leds.setPixelColor(cape_matrix[x][y], (intensity,intensity,intensity));
        }
  
        for (int16_t i=0; i < STRIPS_TOTAL; i++) {
          progressive_fade_off(i, fade);
        }
              leds.show();
            delay(delay_func());
    }
  } else {
    for (int x = COLLAR_HEIGHT-1; x > 0; x--) {
      for (int y = 0; y < COLLAR_WIDTH; y++) {
    
          leds.setPixelColor(collar_matrix[y][x], (intensity,intensity,intensity));
        }
  
        for (int16_t i=0; i < STRIPS_TOTAL; i++) {
          progressive_fade_off(i, fade);
        }
              leds.show();
            delay(delay_func() * 2);
  }
}
}

void wipe_down(int intensity, int fade, boolean collar) {
   for (int16_t i=0; i < STRIPS_TOTAL; i++) {
      progressive_fade_off(i, fade);
  }
  if (!collar) {
    for (int x = 0; x < CAPE_HEIGHT; x++) {
      for (int y = 0; y < CAPE_WIDTH; y++) {
         leds.setPixelColor(cape_matrix[x][y], (intensity,intensity,intensity));
      }
  
      for (int16_t i=0; i < STRIPS_TOTAL; i++) {
        progressive_fade_off(i, fade);
      }
          leds.show();
          delay(delay_func());
    }
  } else {
    for (int x = 0; x < COLLAR_HEIGHT; x++) {
      for (int y = 0; y < COLLAR_WIDTH; y++) {
         leds.setPixelColor(collar_matrix[y][x], (intensity,intensity,intensity));
      }
  
      for (int16_t i=0; i < STRIPS_TOTAL; i++) {
        progressive_fade_off(i, fade);
      }
          leds.show();
          delay(delay_func() * 2);
    }
  }
}

void fade_entire_matrix_off(int fade_rate) { 
  uint16_t uv_value = 1;
  boolean success = false;
  boolean first_success;
  uint16_t uv_fade;
  while (success == false) { // run until off
    first_success = true;
    for(uint16_t i = 0; i < STRIPS_TOTAL; i++) { 
        uv_value = (leds.getPixelColor(i));
        if ((leds.getPixelColor(i) >> 16) > 0) {
          first_success = false;
        }
        //uint16_t LED1 = (leds.getPixelColor(i) >> 16);
        //uint16_t LED2 = (leds.getPixelColor(i) >> 8);
        //uint16_t LED3 = (leds.getPixelColor(i));
        if (uv_value < fade_rate) {
          uv_value = 0;
          uv_fade = 0;
        } else {
          uv_fade = (uv_value - fade_rate);
        }
        leds.setPixelColor(i, (uv_fade, uv_fade, uv_fade));
    }
    leds.show();
    delay(delay_func());
    if (first_success = true) {
      success = true;
    }
  }
};


void progressive_fade_up_or_down(uint8_t new_uv_value) {
  // this one does the entire display at once
      uint8_t current_uv_value;
      leds.show();
      uint32_t current_led = leds.getPixelColor(0); // get the current colour from a single pixel
      current_uv_value = current_led; // separate into RGB components

      while (current_uv_value != new_uv_value){  // while the curr color is not yet the target color
        // doesn't need error correction because we'll never go above or below 0-255
        if (current_uv_value < new_uv_value) {
          current_uv_value++;
        } else if (current_uv_value > new_uv_value) {
          current_uv_value--;  // increment or decrement the old color values
        }
        for(uint16_t i = 0; i < STRIPS_TOTAL; i++) {
          leds.setPixelColor(i, (current_uv_value, current_uv_value, current_uv_value));  // set the color
        }
        delay((delay_func()));
        leds.show();
   }
}

void progressive_fade_off(uint16_t led_num, byte fade_speed) {
    uint32_t current_uv_pixels;
    uint16_t new_uv_value;
    int current_uv_value;
    
    current_uv_pixels = leds.getPixelColor(led_num);

    current_uv_value = current_uv_pixels & 0xFF;

    if ((current_uv_value - fade_speed) > 0) {
      new_uv_value = current_uv_value - fade_speed;
    } else {
      new_uv_value = 0;
    }
    //new_uv_value = (current_uv_value<=fade_speed) ? 0 : (uint16_t) current_uv_value-(current_uv_value*fade_speed/256);
    
    leds.setPixelColor(led_num, (new_uv_value,new_uv_value,new_uv_value));

}
