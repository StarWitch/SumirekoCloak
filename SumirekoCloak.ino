/*
 *  Sumireko Usami Cloak Firmware
 *  by Alice D
 * 
 *  Using: 
 *  * Adafruit Feather M0 Express
 *  * Adafruit Feather NeoPXL87
 *  * Adafruit Feather Hallowing
 *  * Adafruit INA219 Breakout Board
 *  * Adafruit I2C OLED Display
 *  * DFRobot DC-DC Power Module 25W (x2)
 *  * KitterLabs RobotEars v1.0 (self-made part)
 *  * 16 meters WS2811 UV LEDs
 * 
 */

#include <I2C_DMAC.h>
#include <Adafruit_NeoPXL8.h>
#include <Adafruit_INA219.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_LIS3DH.h>
#include <Adafruit_Sensor.h>

// mapping for the cape LEDs
// there are 14 strips on the cloak, at an irregular strip on the collar
// LEDs starting at 448 are on the collar in one continuous strip

#define LEDS_PER_STRIP 64  // indexed at 1 / multiply by 8, as per NeoPXL8 documentation
#define CAPE_HEIGHT 32 // indexed at zero
#define CAPE_WIDTH 14 // indexed at zero
#define COLLAR_HEIGHT 4 // indexed at zero
#define COLLAR_WIDTH 24 // indexed at zero
#define STRIPS_TOTAL 512 // indexed at zero, total for whole array

// SDA pin (I2C) remapped to MOSI
int8_t pins[8] = { A3, MOSI, A4, 5, MISO, PIN_SERIAL1_TX, 13, PIN_SERIAL1_RX};

Adafruit_NeoPXL8 leds(LEDS_PER_STRIP, pins, NEO_GRB + NEO_KHZ800);

Adafruit_INA219 ina219;

Adafruit_LIS3DH lis = Adafruit_LIS3DH();

#define OLED_RESET     4
Adafruit_SSD1306 display(128, 32, &Wire, OLED_RESET);

void setup() {
  // A5 = Potentiometer
  // 11 = Button (internal pulldown! set this!)
  //pinMode(11, INPUT_PULLUP); // physical button on side of MCU case (doesn't work???)
  pinMode(A5, INPUT); // knob on side of MCU case
 //attachInterrupt(A5, input_checks, CHANGE);
  randomSeed(analogRead(A5));
  // LEDs
  leds.begin();
  //brightness = (analogRead(A5) / 4);
  //leds.setBrightness(brightness);
  leds.setBrightness(255);
  Serial.begin(9600);
  // I2C section
  // completely off until any of this stuff is connected
  if (false) {
    I2C.begin(); // begin I2C bus

    // ina219 board monitors the battery line in specifically
    ina219.begin(); 

    // display for montioring voltage and other data
    display.begin(SSD1306_SWITCHCAPVCC, 0x3C); // begin display
    display.clearDisplay();
    display.display();
    display.setRotation(0);
    display.setTextSize(2);
    display.setTextColor(WHITE);


    // wait, is this already inside the main case???
    lis.begin(0x18);
    lis.setRange(LIS3DH_RANGE_4_G);
    lis.setClick(2, 500);
  
    //Serial.begin(9600);
  };
};

uint32_t frameCount;

template<class T>
void looper(T func, int cycles) {
  frameCount=10000;  // arbitrary seed to calculate the three time displacement variables t,t2,t3

  for (int i = 0; i < cycles; i++) {
    func();
  }
}

void loop() {
  //simple_test();
  int default_cycles = 5;
  int n = random(1, 5);

  switch (n) {
    case 1:
      looper(new_plasma, default_cycles); // looks fantastic, thank fuck
      break;
    case 2:
      looper(sparkle, default_cycles); // looks okay!    
      break;
    case 3:
      looper(matrix_shimmer, default_cycles); //flickers, but looks ok
      break;
    case 4:
      looper(fade_wipe, default_cycles);
      break;
  }
  progressive_fade_up_or_down(1);
    
};
