
// INA219 / SSD1306 section
// mostly taken from their example code 

uint32_t total_sec = 0;
float total_mA = 0.0;

void power_check() {
  // Read voltage and current from INA219.
  float shuntvoltage = ina219.getShuntVoltage_mV();
  float busvoltage = ina219.getBusVoltage_V();
  float current_mA = ina219.getCurrent_mA();
  
  // Compute load voltage, power, and milliamp-hours.
  float loadvoltage = busvoltage + (shuntvoltage / 1000);  
  float power_mW = loadvoltage * current_mA;
  total_mA += current_mA;
  total_sec += 1;
  float total_mAH = total_mA / 3600.0;  
  
  // Update display.
  display.clearDisplay();
  display.setCursor(0,0);
  int mode = (total_sec / 5) % 2;
  if (mode == 0) {
    // Mode 0, display volts and amps.
    printSIValue(loadvoltage, "V:", 2, 10);
    display.setCursor(0, 16);
    printSIValue(current_mA/1000.0, "A:", 5, 10);
  }
  else {
    // Mode 1, display watts and milliamp-hours.
    printSIValue(power_mW/1000.0, "W:", 5, 10);
    display.setCursor(0, 16);
    printSIValue(total_mAH/1000.0, "Ah:", 5, 10);
  }
  display.display();

  
}

void printSIValue(float value, char* units, int precision, int maxWidth) {
  // Print a value in SI units with the units left justified and value right justified.
  // Will switch to milli prefix if value is below 1.
  
  // Add milli prefix if low value.
  if (fabs(value) < 1.0) {
    display.print('m');
    maxWidth -= 1;
    value *= 1000.0;
    precision = max(0, precision-3);
  }
  
  // Print units.
  display.print(units);
  maxWidth -= strlen(units);
  
  // Leave room for negative sign if value is negative.
  if (value < 0.0) {
    maxWidth -= 1;
  }
  
  // Find how many digits are in value.
  int digits = ceil(log10(fabs(value)));
  if (fabs(value) < 1.0) {
    digits = 1; // Leave room for 0 when value is below 0.
  }
  
  // Handle if not enough width to display value, just print dashes.
  if (digits > maxWidth) {
    // Fill width with dashes (and add extra dash for negative values);
    for (int i=0; i < maxWidth; ++i) {
      display.print('-');
    }
    if (value < 0.0) {
      display.print('-');
    }
    return;
  }
  
  // Compute actual precision for printed value based on space left after
  // printing digits and decimal point.  Clamp within 0 to desired precision.
  int actualPrecision = constrain(maxWidth-digits-1, 0, precision);
  
  // Compute how much padding to add to right justify.
  int padding = maxWidth-digits-1-actualPrecision;
  for (int i=0; i < padding; ++i) {
    display.print(' ');
  }
  
  // Finally, print the value!
  display.print(value, actualPrecision);
}
